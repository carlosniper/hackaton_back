package com.example.hackaton.back.controllers;

import com.example.hackaton.back.models.UserModel;
import com.example.hackaton.back.services.UserService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;



@RestController
@RequestMapping("/v1/users")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<List<UserModel>> getAll() {
        return new ResponseEntity<>(this.userService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable String id){
        Optional<UserModel> oUserModel = this.userService.findById(id);
        if (oUserModel.isPresent()) {
            return new ResponseEntity<>(oUserModel.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(String.format("User with id %s not found!", id), HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity addUser(@RequestBody UserModel userModel) {
        if (Objects.isNull(userModel)){
            return new ResponseEntity<>("UserModel is mandatory!", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(this.userService.save(userModel), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateUser(@PathVariable String id, @RequestBody UserModel userModel) {
        if (Objects.isNull(userModel)){
            return new ResponseEntity<>("UserModel is mandatory!", HttpStatus.BAD_REQUEST);
        }
        if(!id.equals(userModel.getId())) {
            return new ResponseEntity("User id not are equals!", HttpStatus.BAD_REQUEST);
        }
        Optional<UserModel> oUserModel = this.userService.update(id, userModel);
        if(oUserModel.isPresent()){
            return new ResponseEntity(oUserModel.get(), HttpStatus.OK);
        }
        return new ResponseEntity(String.format("UserModel with id %s not found!", id), HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteUser(@PathVariable String id) {
        if (this.userService.delete(id)) {
            return new ResponseEntity(String.format("UserModel with id %s deleted!", id), HttpStatus.OK);
        }
        else {
            return new ResponseEntity(String.format("UserModel with id %s not found!", id), HttpStatus.NOT_FOUND);
        }
    }
}
