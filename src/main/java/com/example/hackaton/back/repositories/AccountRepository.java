package com.example.hackaton.back.repositories;

import com.example.hackaton.back.models.AccountModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository  extends MongoRepository<AccountModel, String> {
}
