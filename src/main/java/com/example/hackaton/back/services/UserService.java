package com.example.hackaton.back.services;

import com.example.hackaton.back.models.UserModel;
import com.example.hackaton.back.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Optional<UserModel> findById(String id) {
        return this.userRepository.findById(id);
    }

    public List<UserModel> findAll() {
        return this.userRepository.findAll();
    }

    public UserModel save(UserModel userModel) {
        return this.userRepository.save(userModel);
    }

    public Optional<UserModel> update(String id, UserModel userModel) {
        Optional<UserModel> oUserModel = this.userRepository.findById(id);

        if (oUserModel.isPresent()) {
            UserModel userModelDB = oUserModel.get();
            userModelDB.setName(userModel.getName());
            userModelDB.setSurname(userModel.getSurname());
            userModelDB.setPhone(userModel.getPhone());
            return Optional.of(this.userRepository.save(userModelDB));
        }

        return oUserModel;
    }

    public boolean delete(String id) {
        Optional<UserModel> oUserModel = this.userRepository.findById(id);

        if(oUserModel.isPresent()) {
            this.userRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
