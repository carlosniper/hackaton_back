package com.example.hackaton.back.models;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;

public class AccountModelTest {

    @Test
    public void test_accountModel_constructor() {
        AccountModel sut = new AccountModel();

        Assert.assertNotNull(sut);
        Assert.assertNull(sut.getAccount());
        Assert.assertEquals(sut.getAmount(), 0f, 0f);
        Assert.assertNull(sut.getIdUser());
        Assert.assertNull(sut.getMovements());
    }

    @Test
    public void test_accountModel_constructor_arguments() {
        AccountModel sut = new AccountModel("account", "idUser", 29.99f,
                Collections.singletonMap("key", 12.99f));

        Assert.assertNotNull(sut);
        Assert.assertEquals(sut.getAccount(), "account");
        Assert.assertEquals(sut.getIdUser(), "idUser");
        Assert.assertEquals(sut.getAmount(), 29.99f, 0f);
        Assert.assertEquals(sut.getMovements(), Collections.singletonMap("key", 12.99f));
    }

    @Test
    public void test_accountModel_setters_getters() {

        AccountModel sut = new AccountModel();

        sut.setAccount("account");
        sut.setIdUser("idUser");
        sut.setAmount(29.99f);
        sut.setMovements(Collections.singletonMap("key", 12.99f));

        Assert.assertNotNull(sut);
        Assert.assertEquals(sut.getAccount(), "account");
        Assert.assertEquals(sut.getIdUser(), "idUser");
        Assert.assertEquals(sut.getAmount(), 29.99f, 0f);
        Assert.assertEquals(sut.getMovements(), Collections.singletonMap("key", 12.99f));
    }
}
