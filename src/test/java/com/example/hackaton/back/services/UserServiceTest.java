package com.example.hackaton.back.services;

import com.example.hackaton.back.models.UserModel;
import com.example.hackaton.back.repositories.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserService userService;

    @Test
    public void test_userService_findById() {
        UserModel userModel = new UserModel("id", "name", "surname", "phone");
        Mockito.when(this.userRepository.findById("id")).thenReturn(Optional.of(userModel));

        Optional<UserModel> result = this.userService.findById("id");

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(result.get().getId(), "id");
        Assert.assertEquals(result.get().getName(), "name");
        Assert.assertEquals(result.get().getSurname(), "surname");
        Assert.assertEquals(result.get().getPhone(), "phone");
    }

    @Test
    public void test_userService_findAll() {
        Mockito.when(this.userRepository.findAll()).thenReturn(
                Arrays.asList(new UserModel("id", "name", "surname", "phone"),
                        new UserModel("id1", "name1", "surname1", "phone1")));

        List<UserModel> result = this.userService.findAll();

        Assert.assertEquals(result.size(), 2);
        Assert.assertEquals(result.get(0).getId(), "id");
        Assert.assertEquals(result.get(0).getName(), "name");
        Assert.assertEquals(result.get(0).getSurname(), "surname");
        Assert.assertEquals(result.get(0).getPhone(), "phone");
    }

    @Test
    public void test_userService_save() {
        UserModel userModel = new UserModel("id", "name", "surname", "phone");
        Mockito.when(this.userRepository.save(userModel)).thenReturn(userModel);

        UserModel result = this.userService.save(userModel);

        Assert.assertNotNull(result);
        Assert.assertEquals(result.getId(), "id");
        Assert.assertEquals(result.getName(), "name");
        Assert.assertEquals(result.getSurname(), "surname");
        Assert.assertEquals(result.getPhone(), "phone");
    }

    @Test
    public void test_userService_update_ok() {
        String id = "id";
        UserModel userModel = new UserModel(id, "name", "surname", "phone");
        Mockito.when(this.userRepository.save(userModel)).thenReturn(userModel);
        Mockito.when(this.userRepository.findById(id)).thenReturn(Optional.of(userModel));

        Optional<UserModel> result = this.userService.update(id, userModel);

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(result.get().getId(), "id");
        Assert.assertEquals(result.get().getName(), "name");
        Assert.assertEquals(result.get().getSurname(), "surname");
        Assert.assertEquals(result.get().getPhone(), "phone");
    }

    @Test
    public void test_userService_update_notfound() {
        String id = "id";
        UserModel userModel = new UserModel(id, "name", "surname", "phone");
        Mockito.when(this.userRepository.findById(id)).thenReturn(Optional.empty());

        Optional<UserModel> result = this.userService.update(id, userModel);

        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void test_userService_delete_ok() {
        String id = "id";
        UserModel userModel = new UserModel(id, "name", "surname", "phone");
        Mockito.when(this.userRepository.findById(id)).thenReturn(Optional.of(userModel));

        boolean result = this.userService.delete(id);

        Mockito.verify(this.userRepository, times(1)).findById(id);

        Assert.assertTrue(result);
    }

    @Test
    public void test_userService_delete_notfound() {
        String id = "id";
        Mockito.when(this.userRepository.findById(id)).thenReturn(Optional.empty());

        boolean result = this.userService.delete(id);

        Assert.assertFalse(result);

    }
}
