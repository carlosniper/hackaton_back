package com.example.hackaton.back.services;

import com.example.hackaton.back.models.AccountModel;
import com.example.hackaton.back.models.UserModel;
import com.example.hackaton.back.repositories.AccountRepository;
import com.example.hackaton.back.repositories.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    AccountRepository accountRepository;

    @Mock
    UserRepository userRepository;

    @InjectMocks
    AccountService accountService;

    @Test
    public void test_accountService_findAll() {
        Mockito.when(this.accountRepository.findAll()).thenReturn(
                Arrays.asList(new AccountModel("account", "idUser", 29.99f, Collections.singletonMap("key", 12.99f)),
                        new AccountModel("account1", "idUser1", 39.99f, Collections.singletonMap("key1", 22.99f))));

        List<AccountModel> result = this.accountService.findAll();

        Assert.assertEquals(result.size(), 2);
        Assert.assertEquals(result.get(0).getAccount(), "account");
        Assert.assertEquals(result.get(0).getIdUser(), "idUser");
        Assert.assertEquals(result.get(0).getAmount(), 29.99f, 0f);
        Assert.assertEquals(result.get(0).getMovements(), Collections.singletonMap("key", 12.99f));
    }

    @Test
    public void test_accountService_findById() {
        String id = "id";
        AccountModel accountModel = new AccountModel("account", "idUser", 29.99f, Collections.singletonMap("key", 12.99f));
        Mockito.when(this.accountRepository.findById(id)).thenReturn(Optional.of(accountModel));

        Optional<AccountModel> result = this.accountService.findById(id);

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(result.get().getAccount(), "account");
        Assert.assertEquals(result.get().getIdUser(), "idUser");
        Assert.assertEquals(result.get().getAmount(), 29.99f, 0f);
        Assert.assertEquals(result.get().getMovements(), Collections.singletonMap("key", 12.99f));
    }

    @Test
    public void test_accountService_findById_notfound() {
        String id = "id";
        Mockito.when(this.accountRepository.findById(id)).thenReturn(Optional.empty());

        Optional<AccountModel> result = this.accountService.findById(id);

        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void test_accountService_add_ok() {
        UserModel userModel = new UserModel();
        AccountModel accountModel = new AccountModel("account", "idUser", 29.99f, Collections.singletonMap("key", 12.99f));

        Mockito.when(this.userRepository.findById("idUser")).thenReturn(Optional.of(userModel));
        Mockito.when(this.accountRepository.findById("account")).thenReturn(Optional.empty());

        AccountServiceResponse result = this.accountService.add(accountModel);

        Mockito.verify(this.accountRepository, times(1)).save(accountModel);

        Assert.assertEquals(result.getHttpStatus(), HttpStatus.CREATED);
        Assert.assertEquals(result.getMsg(), "Cuenta creada correctamente");
        Assert.assertEquals(result.getAccount(), accountModel);
    }

    @Test
    public void test_accountService_add_user_notfound() {
        AccountModel accountModel = new AccountModel("account", "idUser", 29.99f, Collections.singletonMap("key", 12.99f));

        Mockito.when(this.userRepository.findById("idUser")).thenReturn(Optional.empty());

        AccountServiceResponse result = this.accountService.add(accountModel);

        Assert.assertEquals(result.getHttpStatus(), HttpStatus.BAD_REQUEST);
        Assert.assertEquals(result.getMsg(), "El usuario no existe por lo que no se puede crear la cuenta");
        Assert.assertEquals(result.getAccount(), accountModel);
    }

    @Test
    public void test_accountService_add_account_exist() {
        UserModel userModel = new UserModel();
        AccountModel accountModel = new AccountModel("account", "idUser", 29.99f, Collections.singletonMap("key", 12.99f));

        Mockito.when(this.userRepository.findById("idUser")).thenReturn(Optional.of(userModel));
        Mockito.when(this.accountRepository.findById("account")).thenReturn(Optional.of(accountModel));

        AccountServiceResponse result = this.accountService.add(accountModel);

        Assert.assertEquals(result.getHttpStatus(), HttpStatus.BAD_REQUEST);
        Assert.assertEquals(result.getMsg(), "Ya hay una cuenta con este identificador");
        Assert.assertEquals(result.getAccount(), accountModel);
    }

    @Test
    public void test_accountService_update_ok() {
        AccountModel accountModel = new AccountModel("account", "idUser", 29.99f, Collections.singletonMap("key", 12.99f));
        Mockito.when(this.accountRepository.findById("account")).thenReturn(Optional.of(accountModel));
        UserModel userModel = new UserModel();
        Mockito.when(this.userRepository.findById("idUser")).thenReturn(Optional.of(userModel));

        AccountServiceResponse result = this.accountService.update("account", accountModel);

        Mockito.verify(this.accountRepository, times(1)).save(accountModel);

        Assert.assertEquals(result.getHttpStatus(), HttpStatus.CREATED);
        Assert.assertEquals(result.getMsg(), "Cuenta modificada correctamente");
        Assert.assertEquals(result.getAccount(), accountModel);
    }

    @Test
    public void test_accountService_update_id_notequals() {
        AccountModel accountModel = new AccountModel("account", "idUser", 29.99f, Collections.singletonMap("key", 12.99f));

        AccountServiceResponse result = this.accountService.update("1", accountModel);

        Assert.assertEquals(result.getHttpStatus(), HttpStatus.BAD_REQUEST);
        Assert.assertEquals(result.getMsg(), "No coinciden la Cuenta de la URL y la del Body");
        Assert.assertNull(result.getAccount());
    }

    @Test
    public void test_accountService_update_account_notfound() {
        AccountModel accountModel = new AccountModel("account", "idUser", 29.99f, Collections.singletonMap("key", 12.99f));
        Mockito.when(this.accountRepository.findById("account")).thenReturn(Optional.empty());

        AccountServiceResponse result = this.accountService.update("account", accountModel);

        Assert.assertEquals(result.getHttpStatus(), HttpStatus.NOT_FOUND);
        Assert.assertEquals(result.getMsg(), "Cuenta no existente");
        Assert.assertNull(result.getAccount());
    }

    @Test
    public void test_accountService_update_user_notfound() {
        AccountModel accountModel = new AccountModel("account", "idUser", 29.99f, Collections.singletonMap("key", 12.99f));
        Mockito.when(this.accountRepository.findById("account")).thenReturn(Optional.of(accountModel));
        Mockito.when(this.userRepository.findById("idUser")).thenReturn(Optional.empty());

        AccountServiceResponse result = this.accountService.update("account", accountModel);

        Assert.assertEquals(result.getHttpStatus(), HttpStatus.BAD_REQUEST);
        Assert.assertEquals(result.getMsg(), "El usuario no existe por lo que no se puede crear la cuenta");
        Assert.assertEquals(result.getAccount(), accountModel);
    }

    @Test
    public void test_accountService_partialUpdate_ok_empty_movements() {
        AccountModel accountModel = new AccountModel("account", "idUser", 29.99f, Collections.singletonMap("key", 12.99f));
        AccountModel accountModel2 = new AccountModel("account", null, 0f, null);

        Mockito.when(this.accountRepository.findById("account")).thenReturn(Optional.of(accountModel));

        AccountServiceResponse result = this.accountService.partialUpdate("account", accountModel2);

        Mockito.verify(this.accountRepository, times(1)).save(accountModel);

        Assert.assertEquals(result.getHttpStatus(), HttpStatus.OK);
        Assert.assertEquals(result.getMsg(), "Cuenta actualizada correctamente");
        Assert.assertEquals(result.getAccount(), accountModel2);
    }

    @Test
    public void test_accountService_partialUpdate_ok() {
        AccountModel accountModel = new AccountModel("account", "idUser", 29.99f, Collections.singletonMap("key", 12.99f));
        AccountModel accountModel2 = new AccountModel("account", null, 0f, null);

        Mockito.when(this.accountRepository.findById("account")).thenReturn(Optional.of(accountModel));

        AccountServiceResponse result = this.accountService.partialUpdate("account", accountModel2);

        Mockito.verify(this.accountRepository, times(1)).save(accountModel);

        Assert.assertEquals(result.getHttpStatus(), HttpStatus.OK);
        Assert.assertEquals(result.getMsg(), "Cuenta actualizada correctamente");
        Assert.assertEquals(result.getAccount(), accountModel2);
    }
}
